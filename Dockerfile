FROM ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y apache2
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
RUN ln -sf /dev/stdout /var/log/apache2/access.log && ln -sf /dev/stderr /var/log/apache2/error.log

COPY index.html /var/www/html/index.html

EXPOSE 80
STOPSIGNAL SIGTERM
CMD ["apachectl", "-D", "FOREGROUND"]
